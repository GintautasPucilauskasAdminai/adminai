﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StorageEnterprises.Models
{
    public class Product
    {
        [Display(Name = "Product ID")]
        public int Id { get; set; }

        [Display(Name = "Product Name")]
        [Required(ErrorMessage = "You need to input product name")]
        public string Name { get; set; }

        [Display(Name = "Product Type")]
        [Required(ErrorMessage = "You need to input product type")]
        public string Type { get; set; }

        [Display(Name = "Product Stock")]
        [Required(ErrorMessage = "You need to input product stock")]
        public int Stock { get; set; }

        [Display(Name = "Product Price")]
        [Required(ErrorMessage = "You need to input product price")]
        public double Price { get; set; }

        [Display(Name = "Product Description")]
        [Required(ErrorMessage = "You need to input product description")]
        public string Description { get; set; }

        [Display(Name = "Product Picture")]
        [Required(ErrorMessage = "You need to input product picture link")]
        public string Picture { get; set; }
        //public Product(int id, string name, string type, int stock, double price, string description, string picture)
        //{
        //    this.Id = id;
        //    this.Name = name;
        //    this.Type = type;
        //    this.Stock = stock;
        //    this.Price = price;
        //    this.Description = description;
        //    this.Picture = picture;
        //}
    }
}
