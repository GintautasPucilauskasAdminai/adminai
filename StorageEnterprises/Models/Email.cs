﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace StorageEnterprises.Models
{
    public class Email
    {
        [Required, Display(Name = "Full name")]
        public string FromName { get; set; }
        [Required, Display(Name = "Your contact email"), EmailAddress]
        public string FromEmail { get; set; }
        [Required, Display(Name = "Message")]
        public string Message { get; set; }
    }
}
