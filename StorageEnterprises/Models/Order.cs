﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace StorageEnterprises.Models
{
    //An object that represents individual order instances.
    public class Order
    {
        [Display(Name = "Order ID")]
        [Required]
        public int Id { get; set; }

        [Display(Name = "Client")]
        //[Required(ErrorMessage = "You need to input a client name")]
        public string Client { get; set; }

        [Display(Name = "Product ID")]
        [Required(ErrorMessage = "You need to input product id")]
        public int ProductId { get; set; }

        [Display(Name = "Product count")]
        [Required(ErrorMessage = "You need to input production count")]
        public int Count { get; set; }

        [Display(Name = "Create date")]
        [Required(ErrorMessage = "You need to input creation date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreationDate { get; set; } = DateTime.Now.Date;

        [Required(ErrorMessage = "You need to input status")]
        public string Status { get; set; }

        [Display(Name = "Completion date")]
        [Required(ErrorMessage = "You need to input completion date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CompleteDate { get; set; } = DateTime.Now.Date;

    }
}
