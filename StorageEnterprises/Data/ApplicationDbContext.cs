﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using StorageEnterprises.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace StorageEnterprises.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<StorageEnterprises.Models.Product> Products { get; set; }
        public DbSet<StorageEnterprises.Models.Order> Order { get; set; }
    }
}
