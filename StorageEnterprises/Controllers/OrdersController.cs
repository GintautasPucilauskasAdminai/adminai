﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using StorageEnterprises.Data;
using StorageEnterprises.Models;
using System.Net.Mail;

namespace StorageEnterprises.Controllers
{
    public class OrdersController : Controller
    {
        private readonly ApplicationDbContext _context;

        public OrdersController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Orders
        public async Task<IActionResult> Index()
        {
            return View(await _context.Order.ToListAsync());
        }

        // GET: Orders/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Order
                .FirstOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // GET: Orders/Create
        public IActionResult Create(int id)
        {
            Order order = new Order();
            order.Id = 0;
            order.ProductId = id;
            order.Count = 0;
            order.CreationDate = DateTime.Now;
            order.CompleteDate = DateTime.Now;
            order.Status = "Pending";
            return View(order);
        }

        // POST: Orders/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int id, [Bind("Id,Client,ProductId,Count,CreationDate,Status,CompleteDate")] Order order)
        {
            var product = await _context.Products.FindAsync(order.ProductId);
            if (id == null || id < 0)
            {
                return View(order);
            }
            order.ProductId = id;
            order.Id = 0;
            if (ModelState.IsValid)
            {
                _context.Add(order);
                await _context.SaveChangesAsync();

                string title = "Order created";
                string msg = "Your order has been created and will be checked by an emplyee and hopefully approved. <br /> StorageEnterprises Team";
                string email = order.Client;

                if (order.Count > product.Stock)
                {
                    msg = "Your order has been created, however the amount of prodcuts you have requested is currently not in stock. As soon as there are it will be checked by an employee. <br /> StorageEnterprises Team";
                }

                SendEmail(title, msg, email);

                return RedirectToAction(nameof(Index));
            }
            return View(order);
        }

        // GET: Orders/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Order.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Client,ProductId,Count,CreationDate,Status,CompleteDate")] Order order)
        {
            if (id != order.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(order);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderExists(order.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(order);
        }

        // GET: Orders/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Order
                .FirstOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var order = await _context.Order.FindAsync(id);
            _context.Order.Remove(order);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OrderExists(int id)
        {
            return _context.Order.Any(e => e.Id == id);
        }

        public async Task<IActionResult> Cancel(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Order.FirstOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        [HttpPost, ActionName("Cancel")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CancelConfirmed(int id)
        {
            var order = await _context.Order.FindAsync(id);
            //_context.Order.Remove(order);
            order.CompleteDate = DateTime.Now;
            order.Status = "Canceled";
            await _context.SaveChangesAsync();

            string title = "Order canceled";
            string msg = "Darn. It looks like your order has been canceled. If you are wondering why, feel free to contact us. <br /> StorageEnterprises Team";
            string email = order.Client;
            SendEmail(title, msg, email);

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Complete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Order.FirstOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        [HttpPost, ActionName("Complete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Complete(int id)
        {
            var order = await _context.Order.FindAsync(id);
            //_context.Order.Remove(order);
            order.CompleteDate = DateTime.Now;
            order.Status = "Completed";
            await _context.SaveChangesAsync();

            string title = "Order complete";
            string msg = "Your order has been complete. If it hasn't reached you yet, please contat us immediately. <br /> StorageEnterprises Team";
            string email = order.Client;
            SendEmail(title, msg, email);

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Approve(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Order.FirstOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        [HttpPost, ActionName("Approve")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Approve(int id)
        {
            var order = await _context.Order.FindAsync(id);
            //_context.Order.Remove(order);
            //order.CompleteDate = DateTime.Now;
            order.Status = "Approved";
            await _context.SaveChangesAsync();

            string title = "Order approved";
            string msg = "Congratulations loyal customer! Your order has been approved and will be arriving shortly. <br /> StorageEnterprises Team";
            string email = order.Client;
            SendEmail(title, msg, email);

            return RedirectToAction(nameof(Index));
        }

        public static void SendEmail(string title, string msg, string email)
        {
            MailMessage mail = new MailMessage();
            mail.To.Add(email);
            mail.From = new MailAddress("StorageEnterprisesBot@gmail.com");
            mail.Subject = title;
            mail.Body = msg;
            mail.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential("StorageEnterprisesBot@gmail.com", "thisSt0ngPassw0rd");
            smtp.EnableSsl = true;
            smtp.Send(mail);
        }
    }
}
