﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using StorageEnterprises.Data;
using StorageEnterprises.Models;
using System.Net.Mail;

namespace StorageEnterprises.Controllers
{
    public class EmailController : Controller
    {
        public IActionResult Index()
        {
            Email email = new Email();
            return View(email);
        }
        [HttpPost]
        public IActionResult Index(Email _objModelMail)
        {
            if (ModelState.IsValid)
            {
                MailMessage mail = new MailMessage();
                mail.To.Add("StorageEnterprisesBot@gmail.com");
                mail.From = new MailAddress("StorageEnterprisesBot@gmail.com");
                mail.Subject = "Request to register from " + _objModelMail.FromName + ", Contact email: " + _objModelMail.FromEmail;
                string Body = _objModelMail.Message;
                mail.Body = Body;
                mail.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential("StorageEnterprisesBot@gmail.com", "thisSt0ngPassw0rd");   
                smtp.EnableSsl = true;
                smtp.Send(mail);

                return RedirectToAction("Sent");
            }
            else
            {
                return View();
            }
        }

        public IActionResult Sent()
        {
            return View();
        }
    }
}