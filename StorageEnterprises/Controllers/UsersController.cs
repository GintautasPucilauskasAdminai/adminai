﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using StorageEnterprises.Models;

namespace StorageEnterprises.Controllers
{
    public class UsersController : Controller
    {
        private UserManager<ApplicationUser> userManager;
        public UsersController(UserManager<ApplicationUser> userMgr)
        {
            userManager = userMgr;
        }
        public IActionResult Index()
        {
            return View(userManager.Users);
        }

        public async Task<IActionResult> Delete(string id)
        {
            ApplicationUser user = await userManager.FindByIdAsync(id);
            return View(user);
        }

        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> Delete(string id, ApplicationUser model)
        {
            ApplicationUser user = await userManager.FindByIdAsync(id);
            
            var result = await userManager.DeleteAsync(user);
       
            if (ModelState.IsValid)
                return RedirectToAction(nameof(Index));
            else
                return View();
        }
    }
}
